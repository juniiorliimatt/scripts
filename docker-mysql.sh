#!/bin/bash

docker pull mysql/mysql-server:latest &&
# docker run --name=mysqlDB-server -d mysql/mysql-server:latest 
docker run -p 3306:3306 --name mysqlDB-server -e MYSQL_ROOT_PASSWORD=12345-d mysql &&
apt-get install mysql-client &&
docker logs mysqlDB-server 2>&1 | grep GENERATED &&
docker exec -it mysqlDB-server mysql -uroot -p

# mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '12345';
