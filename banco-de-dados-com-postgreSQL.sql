CREATE DATABASE mercado;

CREATE TABLE cateogria (
  id serial PRIMARY KEY,
  descricao varchar(100) NOT NULL,
  status integer DEFAULT 1
);

DROP TABLE public.cateogria ;

CREATE TABLE cateogria (
  id serial PRIMARY KEY,
  descricao varchar(100) NOT NULL,
  status integer DEFAULT 1
);

ALTER TABLE public.cateogria RENAME TO categorias;
ALTER TABLE public.categoria RENAME TO categorias;

INSERT INTO public.categorias (descricao) VALUES
('teste-atualizar'),
('Comidas-apagar'),
('Bebidas'),
('Doces'),
('Hortifruti'),
('Limpeza'),
('Higiene'),
('Massas'),
('Laticínios'),
('Cereais'),
('Congelados'),
('Carnes');

UPDATE public.categorias SET descricao = 'Perfumaria' WHERE id = 12;
UPDATE public.categorias SET status = 0 WHERE descricao = 'Doces';
UPDATE public.categorias SET status  = 0 WHERE id = 11;

DELETE FROM public.categorias WHERE descricao = 'Comidas';

SELECT descricao, status, id FROM public.categorias c ;

SELECT id, descricao FROM public.categorias c ORDER BY id;
SELECT id, descricao FROM public.categorias c ORDER BY id asc;
SELECT id, descricao FROM public.categorias c ORDER BY id desc;

SELECT * FROM public.categorias c 

SELECT id, descricao FROM public.categorias c WHERE descricao = 'Doces' OR descricao = 'Limpeza';

CREATE TABLE produto (
  id serial PRIMARY KEY,
  descricao varchar(100) NOT NULL,
  quantidade integer NOT NULL,
  valor decimal(10, 2) NOT NULL,
  categoria_id integer NOT NULL,
  status integer DEFAULT 1,
  
  FOREIGN KEY (categoria_id) REFERENCES categoria(id)
  
);

ALTER TABLE public.produto RENAME TO produtos;

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Heineken', 10, 5.65, 1),
  ('Brahma', 10, 4.89, 1),
  ('Vinho', 10, 10.99, 1);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Tomate', 10, 1.65, 4),
  ('Batata', 10, 2.89, 4),
  ('Cheiro-verde', 10, 1.99, 4);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Detergente', 10, 1.65, 5),
  ('Sabão', 10, 2.89, 5),
  ('Q-boa', 10, 1.99, 5);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Sabonete', 10, 1.65, 6),
  ('Shampoo', 10, 2.89, 6),
  ('Papel higiênico', 10, 1.99, 6);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Macarrão spaguetti', 10, 1.65, 7),
  ('Macarrão parafuso', 10, 2.89, 7),
  ('Macarrão ninho', 10, 1.99, 7);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Queijo', 10, 1.65, 8),
  ('Macarrão parafuso', 10, 2.89, 8),
  ('Macarrão ninho', 10, 1.99, 8);

UPDATE public.produtos SET descricao = 'Leite UHT' WHERE id = 23;
UPDATE public.produtos SET descricao = 'Leite em pó' WHERE id = 24;

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Arroz', 10, 1.65, 9),
  ('Feijão', 10, 2.89, 9),
  ('Farinah', 10, 1.99, 9);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Frango congelado', 10, 1.65, 10),
  ('Linguiça congelada', 10, 2.89, 10),
  ('Sorvete', 10, 1.99, 10);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Desodorante Rexona', 10, 1.65, 12),
  ('Perfume Loros', 10, 2.89, 12),
  ('Loção pós barba', 10, 1.99, 12);

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Doce de goiaba', 10, 1.65, 3),
  ('Doce de banana', 10, 2.89, 3),
  ('Doce de mamão', 10, 1.99, 3);

UPDATE public.produtos SET quantidade = 3 WHERE id = 1 OR id = 3 OR id = 7 OR id = 10 OR id = 15;
UPDATE public.produtos SET quantidade = 9 WHERE id = 6 OR id = 6 OR id = 12 OR id = 17 OR id = 20;
UPDATE public.produtos SET quantidade = 15 WHERE id = 13 OR id = 14 OR id = 27 OR id = 33 OR id = 35;
UPDATE public.produtos SET quantidade = 13 WHERE id = 18 OR id = 19 OR id = 32 OR id = 38 OR id = 28;
UPDATE public.produtos SET quantidade = 8 WHERE id = 11 OR id = 16 OR id = 22 OR id = 24 OR id = 29;
UPDATE public.produtos SET quantidade = 5 WHERE id = 2 OR id = 23 OR id = 25 OR id = 30 OR id = 31;

SELECT * FROM public.produtos p ;

INSERT INTO public.produtos (
  descricao, quantidade, valor, categoria_id)
  VALUES 
  ('Patinho', 20, 1.65, 11),
  ('Cupim', 19, 2.89, 11),
  ('Picanha', 13, 1.99, 11);

SELECT * FROM public.produtos p INNER JOIN public.categorias c2 ON p.categoria_id = c2.id;
SELECT * FROM public.produtos p left JOIN public.categorias c ON p.categoria_id = c.id;

SELECT descricao, quantidade, valor, (quantidade * valor) AS total FROM public.produtos p ;

