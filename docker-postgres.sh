#!/bin/bash

echo "criar um container com postgres" &&
sudo docker pull postgres &&
sudo mkdir -p $HOME/docker/volumes/postgres &&
# sudo docker network create --driver bridge postgres-network &&
sudo docker run --name postgresDB -e POSTGRES_PASSWORD=12345 -p 3333:5432 -d -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres

# Criando um container para acesso
# docker run --rm -it --name psql governmentpaas/psql sh -c "psql -h 172.17.0.2 -U postgres --password"